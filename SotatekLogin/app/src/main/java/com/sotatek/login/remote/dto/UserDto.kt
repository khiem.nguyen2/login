package com.sotatek.login.remote.dto

data class UserDto(
    val userId: String,
    val userName: String
)