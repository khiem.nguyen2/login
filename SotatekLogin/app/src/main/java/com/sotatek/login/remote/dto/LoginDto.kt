package com.sotatek.login.remote.dto

data class LoginDto(
    var username: String = "",
    var password: String = ""
)