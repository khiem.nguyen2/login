package com.sotatek.login.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sotatek.login.MyApplication

abstract class BaseActivity : AppCompatActivity(), BaseView {
    val app by lazy {
        application as MyApplication
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getContentViewId())
        afterView()
    }

    override fun afterView() {
    }

    override fun onResume() {
        super.onResume()
        app.setCurrentActivity(this)
    }

}