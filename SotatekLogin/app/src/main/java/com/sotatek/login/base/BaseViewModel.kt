package com.sotatek.login.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sotatek.login.remote.erros.DomainError

abstract class BaseViewModel:ViewModel() {
    val progressBarLD = MutableLiveData(false)
    val errorLD = MutableLiveData<DomainError>()
}