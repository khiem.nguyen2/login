package com.sotatek.login.di

import androidx.room.Room
import com.google.gson.Gson
import com.sotatek.login.MainViewModel
import com.sotatek.login.local.AppDatabase
import com.sotatek.login.remote.Setup
import com.sotatek.login.remote.api.BaseApi
import com.sotatek.login.remote.interceptors.AuthInterceptor
import com.sotatek.login.remote.repo.BaseRepo
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val DB_NAME = "sotatek_db"
private const val TIMEOUT = 30L

val vmModules = module {
    viewModel { MainViewModel(baseRepo = get()) }
}

val netModule = module {
    single {
        GsonConverterFactory.create()
    }

    single(named("noauth")) {
        OkHttpClient.Builder().apply {
            addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }.build()
    }

    single(named("auth")) {/**/
        OkHttpClient.Builder().apply {
            addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            addInterceptor(AuthInterceptor())
            connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            readTimeout(TIMEOUT, TimeUnit.SECONDS)
            writeTimeout(TIMEOUT, TimeUnit.SECONDS)
        }.build()
    }

    single(named("sotatek")) {
        ApiService.get(Setup.BASE_URL, get(), get(named("auth")))
            .create(BaseApi::class.java)
    }
}

val appModule = module {
    single { Gson() }
}

val databaseModule = module {
    single {
        Room.databaseBuilder(
            androidContext(),
            AppDatabase::class.java, DB_NAME
        ).build()
    }
    single {
        get<AppDatabase>().accountDao()
    }
}

val repositoryModule = module {
    single {
        BaseRepo(
            BaseApi = get(named("sotatek")),
            userDao = get<AppDatabase>().accountDao()
        )
    }
}

object ApiService {
    fun get(
        baseUrl: String,
        converterFactory: GsonConverterFactory,
        client: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(converterFactory)
            .client(client)
            .build()
    }
}