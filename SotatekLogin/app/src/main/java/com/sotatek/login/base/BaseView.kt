package com.sotatek.login.base

interface BaseView {
    fun getContentViewId(): Int
    fun afterView()
}