package com.sotatek.login

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.sotatek.login.base.BaseViewModel
import com.sotatek.login.local.entity.User
import com.sotatek.login.remote.Resource
import com.sotatek.login.remote.dto.LoginDto
import com.sotatek.login.remote.repo.BaseRepo
import kotlinx.coroutines.launch

class MainViewModel(private val baseRepo: BaseRepo) : BaseViewModel() {
    var userName: String = ""
    var password: String = ""

    val messageLiveData = MutableLiveData<String>()
    val usersLiveData = baseRepo.allAccount

    fun loginAccount() {
        viewModelScope.launch {
            progressBarLD.postValue(true)
            when (val result = baseRepo.login(LoginDto(userName, password))) {
                is Resource.Success -> {
                    progressBarLD.postValue(false)
                    val xacc = result.data.headers()["X-Acc"]
                    Log.d("x-Acc", "x-Acc $xacc")
                    val body = result.data.body()
                    body?.userDto?.let {
                        messageLiveData.postValue("Welcome " + body.userDto!!.userName)
                        baseRepo.saveAccount(User(it.userId, it.userName, xacc))
                    }
                }
                is Resource.Loading -> progressBarLD.postValue(true)
                is Resource.Failure -> {
                    progressBarLD.postValue(false)
                    messageLiveData.postValue(result.error.message)
                }
            }
        }
    }
}

