package com.sotatek.login.base

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

abstract class BaseFragment : Fragment(), BaseView, CoroutineScope,
    FragmentResult {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(javaClass.name, "onCreate()...")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(javaClass.name, "onCreateView()...")
        return inflater.inflate(getContentViewId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d(javaClass.name, "onViewCreated()...")
        afterView()
    }

    override fun onDestroyView() {
        Log.d(javaClass.name, "onDestroyView()...")
        super.onDestroyView()
    }

    override fun onDestroy() {
        Log.d(javaClass.name, "onDestroy()...")
        super.onDestroy()
    }

    override fun onFragmentResult(className: String, isSuccess: Boolean, data: Any?) {
    }


    /**
     * @return true if Back button was handled
     */
    open fun onBackPressed() = false

    fun applyWindowInsets(view: View) {
        ViewCompat.setOnApplyWindowInsetsListener(view) { _, insets ->
            insets.consumeSystemWindowInsets()
        }
    }

    fun showToast(@StringRes message_id: Int) {
        Toast.makeText(requireContext(), message_id, Toast.LENGTH_LONG).show()
    }

}