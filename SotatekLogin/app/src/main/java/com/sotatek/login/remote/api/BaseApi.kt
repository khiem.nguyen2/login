package com.sotatek.login.remote.api

import com.sotatek.login.remote.dto.LoginDto
import com.sotatek.login.remote.dto.LoginResponseDto
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface BaseApi {
    @POST("login")
    suspend fun login(@Body loginDto: LoginDto): Response<LoginResponseDto>
}