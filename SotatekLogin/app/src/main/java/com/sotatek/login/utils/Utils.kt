package com.sotatek.login.utils

import java.util.regex.Pattern

object Utils {

    private val PATTERN_USERNAME = Pattern.compile(
        "[0-9a-zA-Z]{1,30}"
    )

    private val PATTERN_PASSWORD = Pattern.compile(
        "[0-9a-zA-Z]{1,16}"
    )

    fun isUserNameValid(userName: String): Boolean {
        return PATTERN_USERNAME.matcher(userName).matches()
    }

    fun isPasswordValid(password: String): Boolean {
        return PATTERN_PASSWORD.matcher(password).matches()
    }
}