package com.sotatek.login.local

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import org.koin.core.KoinComponent
import org.koin.core.inject

object AppPreferences : KoinComponent {
    private const val MODE = Context.MODE_PRIVATE
    private lateinit var prefs: SharedPreferences

    val gson: Gson by inject()

    fun init(context: Context) {
        prefs = context.getSharedPreferences(
            context.applicationInfo.loadLabel(context.packageManager).toString()
                .replace("", "_") + "_prefs", MODE
        )
    }

    private const val PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN"
    private const val PREF_KEY_BALANCE = "PREF_KEY_BALANCE"


    var accessToken: String?
        get() = prefs.getString(PREF_KEY_ACCESS_TOKEN, "")
        set(accessToken) = prefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply()

    var balance: Int
        get() = prefs.getInt(PREF_KEY_BALANCE, 300)
        set(value) = prefs.edit().putInt(PREF_KEY_BALANCE, value).apply()

    fun clear() {
        prefs.edit().clear().apply()
    }
}