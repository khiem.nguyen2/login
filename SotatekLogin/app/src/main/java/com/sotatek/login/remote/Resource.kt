package com.sotatek.login.remote

import com.sotatek.login.remote.erros.DomainError


sealed class Resource<T> {
    class Success<T>(val data:T):Resource<T>()
    class Failure<T>(val error: DomainError, val data:T?=null):Resource<T>()
    class Loading<T>(val data: T? = null) : Resource<T>()
}