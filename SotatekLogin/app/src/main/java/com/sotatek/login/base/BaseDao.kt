package com.sotatek.login.base

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

interface BaseDao<E> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(entity: E)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(entities: List<E>)

    @Update
    suspend fun update(entity: E)

    @Update
    suspend fun update(entities: List<E>)

    @Delete
    suspend fun delete(entity: E)

}