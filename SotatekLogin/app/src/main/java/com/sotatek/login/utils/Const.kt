package com.sotatek.login.utils

import java.util.regex.Pattern

object Const{
    val PATTERN_EMAIL_ADDRESS = Pattern.compile(
        "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    )

    fun isEmailValid(email: String): Boolean {
        return PATTERN_EMAIL_ADDRESS.matcher(email).matches()
    }
}