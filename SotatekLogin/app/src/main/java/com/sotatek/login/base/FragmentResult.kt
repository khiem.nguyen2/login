package com.sotatek.login.base

interface FragmentResult {
    fun onFragmentResult(className: String, isSuccess: Boolean, data: Any? = null)
}