package com.sotatek.login.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.sotatek.login.base.BaseDao
import com.sotatek.login.local.entity.User

@Dao
interface UserDao : BaseDao<User> {
    @Query("SELECT * FROM user")
    fun findAll(): LiveData<List<User>>
}