package com.sotatek.login.utils

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import androidx.core.util.Pair
import java.net.NetworkInterface
import java.util.*

object NetworkUtil {
    @JvmStatic
    fun isOnline(context: Context?): Boolean {
        if (context == null) return false
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = connectivityManager.getActiveNetworkInfo()
        return netInfo?.isConnected() ?: false
    }
}
