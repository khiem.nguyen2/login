package com.sotatek.login

import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import com.sotatek.login.base.BaseActivity
import com.sotatek.login.utils.NetworkUtil
import com.sotatek.login.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class MainActivity : BaseActivity() {
    private val mainViewModel: MainViewModel by inject()

    override fun getContentViewId() = R.layout.activity_main

    override fun afterView() {
        mainViewModel.progressBarLD.observe(this, Observer {
            progress.isVisible = it
        })

        mainViewModel.messageLiveData.observe(this, Observer {
            if (it != null) {
                tvMessage.text = it.toString()
            }
        })

        mainViewModel.usersLiveData.observe(this, Observer {
            tvCountUser.text = getString(R.string.count_user, it.size)
        })

        etUsername.addTextChangedListener {
            val text = etUsername.text.toString()
            if (text.isNotEmpty() && Utils.isUserNameValid(text)) {
                mainViewModel.userName = text
                etUsername.error = null
            } else {
                mainViewModel.userName = ""
                etUsername.error = getString(R.string.userName_require)
            }
            enableLogin()
        }

        etPassword.addTextChangedListener {
            val text = etPassword.text.toString()
            if (text.isNotEmpty() && Utils.isPasswordValid(text)) {
                mainViewModel.password = text
                etPassword.error = null
            } else {
                mainViewModel.password = ""
                etPassword.error = getString(R.string.password_require)
            }
            enableLogin()
        }
        enableLogin()
        btLogin.setOnClickListener {
            if (!NetworkUtil.isOnline(this)) {
                tvMessage.text = getString(R.string.exception_connect_timeout)
                return@setOnClickListener
            }
            mainViewModel.loginAccount()
        }
    }

    private fun enableLogin() {
        val isEnableLogin =
            mainViewModel.userName.isNotEmpty() && mainViewModel.password.isNotEmpty()
        btLogin.isEnabled = isEnableLogin
        btLogin.alpha = if (isEnableLogin) 1f else 0.5f
    }
}