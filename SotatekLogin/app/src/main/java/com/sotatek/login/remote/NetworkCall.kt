package com.sotatek.login.remote

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.sotatek.login.remote.erros.BackendTypedError
import com.sotatek.login.remote.erros.DomainError
import com.sotatek.login.remote.erros.BaseStandardError
import org.koin.core.KoinComponent
import org.koin.core.inject
import retrofit2.HttpException
import kotlin.reflect.KClass

inline fun <V> networkCall(
    errorClass: KClass<out BackendTypedError> = BaseStandardError::class,
    action: () -> V
): Resource<V> {
    return try {
        Resource.Success(data = action())
    } catch (httpExeption: HttpException) {
        val httpCode = httpExeption.code()
        val httpMessage = httpExeption.message ?: ""
        val httpBody = httpExeption.response()?.errorBody()?.string()
        try {
            val errorResponse =
                if (errorClass::class != BaseStandardError::class) {
                    try {
                        gson.fromJson(httpBody, errorClass.java)
                    } catch (e: JsonSyntaxException) {
                        gson.fromJson(httpBody, BaseStandardError::class.java)
                    }
                } else gson.fromJson(httpBody, BaseStandardError::class.java)

            val domainError = when (httpBody) {
                null -> DomainError.ApiError(httpCode, httpMessage)
                else -> DomainError.ApiError(
                    httpCode,
                    httpMessage,
                    errorResponse
                )
            }
            Resource.Failure(
                error = domainError
            )
        } catch (e: Exception) {
            Resource.Failure(
                error = when {
                    (e is JsonSyntaxException && httpCode >= 500) -> DomainError.ApiError(
                        httpCode,
                        httpMessage,
                        null
                    )
                    else -> DomainError.SystemException(e)
                }
            )
        }

    } catch (e: Exception) {
        Resource.Failure(
            error = when (e) {
                is java.net.UnknownHostException -> DomainError.NetworkException(e) //Unable to locate the server. Please check your network connection.
                is java.net.ConnectException -> DomainError.NetworkException(e) //Unable to connect to the server. Please check your network connection.
                is java.net.SocketTimeoutException -> DomainError.NetworkException(e) //The connection has timed out. Please try again.
                is java.net.SocketException -> DomainError.NetworkException(e) //There are some problems with the connection. Please try again.
                else -> DomainError.SystemException(e)
            }
        )
    }
}
// Inject  in top-level function (reference: https://stackoverflow.com/a/50840374/2964806)
val gson=object :KoinComponent{
    val im: Gson by inject()
}.im