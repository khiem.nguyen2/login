package com.sotatek.login.remote.dto

import com.google.gson.annotations.SerializedName

data class LoginResponseDto(
    @SerializedName("user")
    var userDto: UserDto?
)