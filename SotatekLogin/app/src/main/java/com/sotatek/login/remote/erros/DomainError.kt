package com.sotatek.login.remote.erros

import com.google.gson.annotations.SerializedName
import com.sotatek.login.R


const val ERRORCODE_SYSTEM = "-100"
const val ERRORCODE_NETWORK_DNS = "-101"
const val ERRORCODE_NETWORK_CONNECTION_TIMEDOUT = "-102"
const val ERRORCODE_NETWORK_SOCKET_TIMEDOUT = "-103"
const val ERRORCODE_NETWORK_SOCKET = "-104"

interface DomainErrorInterface {
    val code: String
    val message: String
}

interface BackendTypedError

data class BaseStandardError(
    @SerializedName("errorMessage") val message: String? = null,
    @SerializedName("errorCode") val code: String? = null
) : BackendTypedError

sealed class DomainError : DomainErrorInterface {
    data class ApiError<T : BackendTypedError>(
        val httpCode: Int,
        val httpMessage: String,
        val error: T? = null
    ) : DomainError() {
        override val code: String
            get() = when (error) {
                is BaseStandardError -> when {
                    error.code != null -> error.code
                    else -> httpCode.toString()
                }
                else -> httpCode.toString()
            }
        override val message: String
            get() = when (error) {
                is BaseStandardError -> error.message ?: ""
                else -> ""
            }
    }

    data class NetworkException(val throwable: Throwable) : DomainError() {

        override val code: String
            get() = when (throwable) {
                is java.net.UnknownHostException -> ERRORCODE_NETWORK_DNS
                is java.net.ConnectException -> ERRORCODE_NETWORK_CONNECTION_TIMEDOUT
                is java.net.SocketTimeoutException -> ERRORCODE_NETWORK_SOCKET_TIMEDOUT
                is java.net.SocketException -> ERRORCODE_NETWORK_SOCKET
                else -> error("unexpected: $throwable")
            }
        override val message
            get() = throwable.javaClass.canonicalName ?: ""

        val errorResource
            get() = when (throwable) {
                is java.net.UnknownHostException -> R.string.exception_unknownhost
                is java.net.ConnectException -> R.string.exception_connect_timeout
                is java.net.SocketTimeoutException -> R.string.exception_socket_timeout
                is java.net.SocketException -> R.string.exception_socket
                else -> error("unexpected: $throwable")
            }
    }

    data class SystemException(val throwable: Throwable) : DomainError() {
        override val code: String
            get() = ERRORCODE_SYSTEM
        override val message: String
            get() = throwable.message.toString()
    }
}
