package com.sotatek.login

import android.app.Activity
import android.app.Application
import com.sotatek.login.di.*
import com.sotatek.login.local.AppPreferences
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApplication : Application() {
    companion object {
        lateinit var instance: MyApplication
    }

    private var mCurrentActivity: Activity? = null

    fun getCurrentActivity(): Activity? {
        return mCurrentActivity
    }

    fun setCurrentActivity(mCurrentActivity: Activity?) {
        this.mCurrentActivity = mCurrentActivity
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        AppPreferences.init(this)
        startKoin {
            androidContext(applicationContext)
            modules(appModule, vmModules, netModule, databaseModule, repositoryModule)
        }
    }
}