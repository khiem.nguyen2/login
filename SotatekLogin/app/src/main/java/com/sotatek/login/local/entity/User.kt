package com.sotatek.login.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "user")
data class User(
    @PrimaryKey(autoGenerate = false)
    @field:SerializedName("userId")
    var userId: String = "",

    @field:SerializedName("userName")
    var userName: String? = null,

    @field:SerializedName("xacc")
    var xacc: String? = null
)

