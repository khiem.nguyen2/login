package com.sotatek.login.remote.repo

import com.sotatek.login.local.dao.UserDao
import com.sotatek.login.local.entity.User
import com.sotatek.login.remote.Resource
import com.sotatek.login.remote.api.BaseApi
import com.sotatek.login.remote.dto.LoginDto
import com.sotatek.login.remote.dto.LoginResponseDto
import com.sotatek.login.remote.erros.BaseStandardError
import com.sotatek.login.remote.networkCall
import retrofit2.Response

class BaseRepo(val BaseApi: BaseApi, private val userDao: UserDao) {

    suspend fun login(loginDto: LoginDto): Resource<Response<LoginResponseDto> > {
        return networkCall(errorClass = BaseStandardError::class) {
            BaseApi.login(loginDto)
        }
    }

    suspend fun saveAccount(user: User) {
        userDao.save(user)
    }

    val allAccount = userDao.findAll()

}