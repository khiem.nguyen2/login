package com.sotatek.login.remote.interceptors

import com.sotatek.login.remote.Setup
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()
            .header("IMSI", Setup.IMSI)
            .header("IMEI", Setup.IMEI)
            .build()
        return chain.proceed(request)
    }
}